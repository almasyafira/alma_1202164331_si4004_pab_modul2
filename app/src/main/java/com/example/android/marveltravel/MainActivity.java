package com.example.android.marveltravel;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Output;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Boolean cek_pp = false;
    LayoutInflater inflater;
    View dialogView;
    AlertDialog.Builder dialog;
    public int saldosementara;
    public TextView jumlahsaldo, topup;
    Button btnbeli;
    TextView tgl, tglpulang;
    DatePickerDialog datePickerDialog;
    TextView waktu, waktupulang;
    TimePickerDialog timePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getIntExtra("saldo", 0)!=0){
            saldosementara = getIntent().getIntExtra("saldo", 0);
            jumlahsaldo.setText(" "+NumberFormat.getNumberInstance(Locale.US).format(saldosementara));
        }
        //top up
        jumlahsaldo = findViewById(R.id.txtjumlah);
        topup = findViewById(R.id.txttopup);
        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //final Dialog dialog = new Dialog(MainActivity.this);
                dialog = new AlertDialog.Builder(MainActivity.this);
                inflater = getLayoutInflater();
                dialogView = inflater.inflate(R.layout.topup, null);
                dialog.setView(dialogView);

                final EditText saldo = (EditText) dialogView.findViewById(R.id.txtsaldo);

                dialog.setPositiveButton("TAMBAH SALDO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saldosementara = Integer.parseInt(jumlahsaldo.getText().toString());
                        saldosementara = saldosementara + Integer.parseInt(saldo.getText().toString());
                        //saldo2 = saldo .getText().toString();
                        jumlahsaldo.setText(Integer.toString(saldosementara));
                        Toast.makeText(getApplicationContext(), "Top Up Berhasil!", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        //switch
        final Switch pp = (Switch) findViewById(R.id.txtswitch);
            pp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        cek_pp = true;
                        ((LinearLayout)findViewById(R.id.buatpulang)).setVisibility(View.VISIBLE);
                    }else {
                        cek_pp = false;
                        ((LinearLayout)findViewById(R.id.buatpulang)).setVisibility(View.GONE);
                    }
                    }
                });


        //datepicker
        tgl = (TextView) findViewById(R.id.txttgl);
        tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                                tgl.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });

        tglpulang = (TextView) findViewById(R.id.pilihtanggalpulang);
        tglpulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                                tglpulang.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });

        //timepicker
        waktu = (TextView) findViewById(R.id.txtwaktu);
        waktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int jam = c.get(Calendar.HOUR_OF_DAY);
                int menit = c.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(MainActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                waktu.setText(hourOfDay + "." + minute);
                            }
                        }, jam, menit, false);
                timePickerDialog.show();
            }
        });

        waktupulang = (TextView) findViewById(R.id.pilihwaktupulang);
        waktupulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int jam = c.get(Calendar.HOUR_OF_DAY);
                int menit = c.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(MainActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                waktupulang.setText(hourOfDay + "." + minute);
                            }
                        }, jam, menit, false);
                timePickerDialog.show();
            }
        });


        btnbeli = (Button) findViewById(R.id.btnbeli);
        btnbeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((EditText) findViewById(R.id.txttiket)).getText().length()==0) {
                    ((EditText) findViewById(R.id.txttiket)).setError("Masukkan jumlah tiket!");
                } else {
                    int harga = 0;
                    String tujuan = ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString();
                    String [] tujuan2 = tujuan.split("-");
                    switch (((Spinner)findViewById(R.id.spinner)).getSelectedItem().toString()){
                        case "Jakarta (Rp85.000)": harga = 85000;
                            break;
                        case "Cirebon (Rp150.000)": harga = 150000;
                            break;
                        case "Bekasi (Rp70.000)": harga = 70000;
                            break;
                    }
                    harga = harga*Integer.valueOf(((EditText) findViewById(R.id.txttiket)).getText().toString());
                    if (cek_pp) {
                        harga = harga *2;
                    }
                    if (harga > saldosementara) {
                        Toast.makeText(getApplicationContext(), "Maaf, saldo kamu kurang. Top Up saldo dulu!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this,  CheckoutActivity.class);
                        intent.putExtra("topup", saldosementara);
                        intent.putExtra("harga", harga);
                        intent.putExtra("tujuan", tujuan2[0]);
                        intent.putExtra("tanggalpergi", tgl.getText().toString() + " - " +
                        waktu.getText().toString());
                        intent.putExtra("tiket", ((EditText) findViewById(R.id.txttiket)).getText().toString());
                        intent.putExtra("CekPP", cek_pp);
                        intent.putExtra("tanggalpulang", tglpulang.getText().toString() + " - " + waktupulang.getText().toString());
                        MainActivity.this.startActivity(intent);
                    }
                }
            }
        });


    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(MainActivity.class.getSimpleName(),"onStart");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(MainActivity.class.getSimpleName(),"onDestroy");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.class.getSimpleName(),"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(MainActivity.class.getSimpleName(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(MainActivity.class.getSimpleName(), "onStop");
    }

}
