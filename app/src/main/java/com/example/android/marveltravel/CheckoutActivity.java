package com.example.android.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class CheckoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        ((TextView)findViewById(R.id.txttotal)).setText(" "+NumberFormat.getNumberInstance(Locale.US).format(getIntent().getIntExtra("harga", 0)));
        ((TextView)findViewById(R.id.txttujuan)).setText(getIntent().getStringExtra("tujuan"));
        ((TextView) findViewById(R.id.txttglberangkat)).setText(getIntent().getStringExtra("tanggalpergi"));
        ((TextView) findViewById(R.id.txtjmltiket)).setText(getIntent().getStringExtra("tiket"));

        if (getIntent().getBooleanExtra("CekPP", false)) {
            ((TextView) findViewById(R.id.txttglpulang)).setText(getIntent().getStringExtra("tanggalpulang"));
        }
    }
    public void klik(View view) {
        int saldo = getIntent().getIntExtra("topup", 0) - getIntent().getIntExtra("harga", 0);
        Intent in = new Intent(this, MainActivity.class);
        in.putExtra("saldo", saldo);
        startActivity(in);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(CheckoutActivity.class.getSimpleName(),"onStart");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(CheckoutActivity.class.getSimpleName(),"onDestroy");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(CheckoutActivity.class.getSimpleName(),"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(CheckoutActivity.class.getSimpleName(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(CheckoutActivity.class.getSimpleName(), "onStop");
    }

}
